import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, BehaviorSubject, of } from "rxjs";

import { Country } from "../models/Countries";

@Injectable({
  providedIn: "root"
})
export class CountriesService {
  private countrySource = new BehaviorSubject<Country>({
    name: "",
    topLevelDomain: [],
    callingCodes: [],
    capital: "",
    region: "",
    subregion: "",
    population: 0,
    latlng: [],
    area: 0,
    gini: 0,
    timezones: [],
    nativeName: "",
    numericCode: "",
    flag: "",
    cioc: ""
  });
  country = this.countrySource.asObservable();

  constructor(private http: HttpClient) {}

  searchCountry(country) {
    const res = this.http.get(
      `https://restcountries.eu/rest/v2/name/${country}`
    );
    return res;
  }
  updateData(data) {
    this.countrySource.next(data);
  }
}

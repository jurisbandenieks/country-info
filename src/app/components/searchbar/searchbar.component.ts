import { Component, OnInit } from "@angular/core";
import { CountriesService } from "../../services/countries.service";

@Component({
  selector: "app-searchbar",
  templateUrl: "./searchbar.component.html",
  styleUrls: ["./searchbar.component.scss"]
})
export class SearchbarComponent implements OnInit {
  searchDetails: string = "";

  constructor(private countriesService: CountriesService) {}

  ngOnInit() {}

  onSubmit() {
    this.countriesService
      .searchCountry(this.searchDetails)
      .subscribe(response => {
        if (response !== null) {
          this.searchDetails = "";
          this.countriesService.updateData(response);
        }
      });
  }
}

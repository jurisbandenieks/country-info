import { Component, OnInit } from "@angular/core";
import { CountriesService } from "../../services/countries.service";

import { Country } from "../../models/Countries";

@Component({
  selector: "app-details",
  templateUrl: "./details.component.html",
  styleUrls: ["./details.component.scss"]
})
export class DetailsComponent implements OnInit {
  countryDetails: Country = {
    name: "",
    topLevelDomain: []
  };

  constructor(private countriesService: CountriesService) {}

  ngOnInit() {
    this.countriesService.country.subscribe(res => {
      this.countryDetails = res[0];
      if (this.countryDetails) {
        this.setFlag();
      }
    });
  }
  setFlag() {
    return {
      "background-image": this.countryDetails.flag
        ? `url(${this.countryDetails.flag})`
        : "transparent"
    };
  }
}

export interface Country {
  name?: String;
  topLevelDomain?: Array<any>;
  callingCodes?: Array<any>;
  capital?: String;
  region?: String;
  subregion?: String;
  population?: Number;
  latlng?: Array<any>;
  area?: Number;
  gini?: Number;
  timezones?: Array<any>;
  nativeName?: String;
  numericCode?: String;
  flag?: String;
  cioc?: String;
}
